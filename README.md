# Gitlab Duo Coffee Chat: Get started with C# and use code task /explain, /refactor, /tests prompts

- Host: @dnsmichi
- Guest: @leetickett-gitlab

## Recording

[![GitLab Duo Coffee Chat](https://img.youtube.com/vi/AdRtX9L--Po/0.jpg)](https://go.gitlab.com/yx7NG3)

## Summary

We asked GitLab Duo Chat how to start a new C# project and learned how to use the dotnet CLI. The first Code Suggestions challenge is not tiny - query the GitLab REST API and print the response. The code works -- we asked Chat for help with .gitignore and CI/CD configuration for C#. The CI/CD jobs fail, and Root Cause Analysis helps out.

Next, we explore code tasks and let Chat /explain the REST API calls in the source code. To create tests later, we ask GitLab Duo to /refactor the selected code into a function. We refined the prompt to `/refactor into the public class` to get code that can be accessed from the test project. The code gets /tests generated using a code task in Duo Chat.

We also learned that C# solutions should contain the application and test project side-by-side to avoid import inception problems. After solving them, we added more tests with the help of code suggestions. The coffee chat concludes with running tests in CI/CD, our first C# solution with the help of GitLab Duo.


## Resources

- YouTube Playlist: https://go.gitlab.com/xReaA1 
- GitLab group with source code projects: https://go.gitlab.com/uTwX11 
- GitLab Duo: https://go.gitlab.com/Z1vBGD 
- Documentation: https://go.gitlab.com/rSbrTI 
    - GitLab Duo Code Suggestions: https://go.gitlab.com/XIuZ5q 
    - GitLab Duo Chat: https://go.gitlab.com/XLpq9a 
- Talk: Efficient DevSecOps Workflows with a little help from AI: https://go.gitlab.com/T864XF 
- Code Suggestions feedback issue: https://go.gitlab.com/07r1sv 
- Duo Chat feedback issue: https://go.gitlab.com/XTU14S 
- GitLab Community Forum: https://go.gitlab.com/GfTthQ
- GitLab Discord: https://go.gitlab.com/YLWVjM 
