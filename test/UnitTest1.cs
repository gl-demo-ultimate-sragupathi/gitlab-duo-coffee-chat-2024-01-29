using System.Text.Json;

namespace test;

public class UnitTest1
{
    [Fact]
    public async Task GetApiResponse_ValidUrl_ReturnsResponse()
    {
        var result = await Program.GetApiResponse("https://example.com");

        Assert.NotNull(result);
    }

    [Fact]
    public async Task GetApiResponse_InvalidUrl_ThrowsException()
    {
        await Assert.ThrowsAsync<HttpRequestException>(() =>
            Program.GetApiResponse("https://invalid.url"));
    }

    // Test the response from Program.GetApiResponse from gitlab.com contains the world DevSec
    [Fact]
    public async Task GetApiResponse_FromGitlab_ContainsDevSec()
    {
        var result = await Program.GetApiResponse("https://gitlab.com");

        Assert.Contains("DevSecOps", result);
    }

    // Test the response from Program.GetApiResponse from gitlab.com/api/v4/projects

    [Fact]
    public async Task GetApiResponse_FromGitlabApiV4Projects_ReturnsProjects()
    {
        var result = await Program.GetApiResponse("https://gitlab.com/api/v4/projects");

        // print the result in a readable format

        var jsonResult = JsonDocument.Parse(result);
        Console.WriteLine(jsonResult.RootElement.ToString());

        Assert.Contains("id", result);
    }
}