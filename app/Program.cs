﻿public class Program
{

    public static async Task<string> GetApiResponse(string url)
    {
        using HttpClient client = new HttpClient();
        var response = await client.GetAsync(url);
        return await response.Content.ReadAsStringAsync();
    }

    public static void Main()
    {
        Console.WriteLine("Hello, World!");

        var gitlabResponse = GetApiResponse("https://gitlab.com/api/v4/projects/54355274").Result;
        Console.WriteLine(gitlabResponse);
    }

}